add someone to the server
  get a free plex account
    https://www.plex.tv/downloads/
      click get an app
        choose the app for your prefverred platform(s)
          if you have a roku you will need to get the roku app but
            i also recomend the mobile app as it can make the experience even better than just with the roku alone.
        
      back on the downloads page
        click singup in the upper right corner of the page
          fill out the info to get your own plex account
        once you have finished the signup process, take note of your plex username so you can share it with sysfs
  get trakt account
    go to https://trakt.tv/
      click join trakt for free in the middle of the page
      fill out the information to create a trakt account
      once logged in with your new username 
        navigate to https://trakt.tv/users/sysfs
          select follow in the middle of the page
        then hobver the mouse ofver your username in the top right corner and select lists in the dropdown that appears
          click "+ add list" near the top left of the page
          create the following PUBLIC lists:
            Movie Queue
              you will use this list to inform sysfs of the movies you would like to see
            Favorite TV
              you will use this list to inform sysfs of the tv shows that you would like to watch, if they have already ended.
            Following TV
              you will use this list to inform sysfs of the tv shows you would like to watch, if new episodes are still airing.
    be prepared to visit https://trakt.tv/activate when sysfs has an opportunity to add you to the system. he will call/text you with a pin.
    Tag any movies that you would like downloaded in the movie queue list, tv shows that are still being made in following tv, and old shows that you would like downloaded in their entirety in favorite tv. 
  
  send an email to sysfs to complete the process.
    email should contain the following information
      real name (First and Last)
      Plex Username so you can be invited to the server
      Trakt Username so sysfs can follow you back on trakt
      Email Address that you would like sysfs to add for Slack notifications
      Times during the next week that you will be available to authorize sysfs to utilize your Trakt account. you will need to have access to the internet as you will have to sign into trakt within a ten minute window and enter the code sysfs gives you.
      
  once i have your email address and your trakt account has been added to  the server; i will send you an invitation to join the Slack notifications channel. please check your email for the invite and proceed as indicated.
  get slack account
  
    https://sisfsplex.slack.com
    once logged in and finished with the intoductory tutorial click on #flexgetdl in the left side-bar to follow what is being grabbed by flexget on a daily basis. 
    
   if you download the Slack mobile app you can receive notificatioons wherever you are and always stay informed of upcoming additions to the server.
  
  